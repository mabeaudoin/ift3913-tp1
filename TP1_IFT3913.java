import gui.CtrlPanel;
import gui.ViewPanel;

import javax.swing.*;
import java.awt.*;

/**
 * classe principale pour le logiciel de parsing du TP1 de IFT 3919, automne 2016.
 *
 * @author Marc-André Beaudoin
 * @author Étienne Boucher
 * @version 1.0
 * @since 2016-09-25
 */
public class TP1_IFT3913 {
    /**
     * methode main pour le parser du TP1 de IFT 3919. Cr�e un frame pour contenir les JPanels d'affichage
     *
     * @param Args inutilisé
     */
    public static void main(String[] Args) {

        //Cr�ation du JFrame
        JFrame frame = new JFrame("TP1 IFT3919");

        //Ajout des duex JPanel de l'affichage
        ViewPanel viewPanel = new ViewPanel();
        CtrlPanel ctrlPanel = new CtrlPanel(viewPanel);

        //Organisation du JFrame
        frame.add(ctrlPanel, BorderLayout.NORTH);
        frame.add(viewPanel, BorderLayout.SOUTH);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        //Positionne le frame au centre de l'écran
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);

    }
}