package gui;

import elementsClasse.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Iterator;

/**
 * classe faisant la gestion des l'affichage des informations sur le diagramme
 * UML
 *
 * @author Marc-André Beaudoin
 * @author Étienne Boucher
 * @version 1.0
 * @since 2016-09-25
 */
public class ViewPanel extends JPanel {
    // Variables
    private HashMap<String, Classe> classes;
    private JList<String> classes_liste;
    private JList<String> attributs_liste;
    private JList<String> methodes_liste;
    private JList<String> sousClasses_liste;
    private JList<String> associationAggregation_liste;
    private JTextArea details_area;
    private Classe classeSelectionnee;

    /**
     * Constructeur de la classe ViewPanel
     */
    public ViewPanel() {
        // L'affichage est organisé dans un GridBagLayout pour rendre
        // l'organisation flexible
        this.setLayout(new GridBagLayout());

        // Section "classes"
        JLabel classes = new JLabel("Classes");
        addToPanel(classes, 200, 30, 1, 1, 0, 0);

        classes_liste = new JList<String>();
        classes_liste.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList tempList = (JList) mouseEvent.getSource();
                int index = tempList.locationToIndex(mouseEvent.getPoint());
                Object o = tempList.getModel().getElementAt(index);
                updateDisplayFromClass(o.toString());
            }
        });
        addToPanel(classes_liste, 200, 200, 1, 5, 0, 1);

        // Section "attributs"
        JLabel attributs = new JLabel("Attributs");
        addToPanel(attributs, 200, 30, 1, 1, 1, 0);

        attributs_liste = new JList<String>();
        attributs_liste.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList tempList = (JList) mouseEvent.getSource();
                int index = tempList.locationToIndex(mouseEvent.getPoint());
                Object o = tempList.getModel().getElementAt(index);
                updateDetails(o.toString(), "attribut");

            }
        });
        addToPanel(attributs_liste, 200, 200, 1, 1, 1, 1);

        // Section "méthodes"
        JLabel methods = new JLabel("Méthodes");
        addToPanel(methods, 300, 30, 1, 1, 2, 0);

        methodes_liste = new JList<String>();
        methodes_liste.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList tempList = (JList) mouseEvent.getSource();
                int index = tempList.locationToIndex(mouseEvent.getPoint());
                Object o = tempList.getModel().getElementAt(index);
                updateDetails(o.toString(), "methode");
            }
        });
        addToPanel(methodes_liste, 300, 200, 1, 1, 2, 1);

        // Section "sous-classes"
        JLabel sousClasses = new JLabel("Sous-classes");
        addToPanel(sousClasses, 200, 30, 1, 1, 1, 2);

        sousClasses_liste = new JList<String>();
        sousClasses_liste.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList tempList = (JList) mouseEvent.getSource();
                int index = tempList.locationToIndex(mouseEvent.getPoint());
                Object o = tempList.getModel().getElementAt(index);
                updateDetails(o.toString(), "sousClasse");
            }
        });
        addToPanel(sousClasses_liste, 200, 200, 1, 1, 1, 3);

        // Section "association/aggrégation
        JLabel associationAggregation = new JLabel("Associations/aggrégations");
        addToPanel(associationAggregation, 300, 30, 1, 1, 2, 2);

        associationAggregation_liste = new JList<String>();
        associationAggregation_liste.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList tempList = (JList) mouseEvent.getSource();
                int index = tempList.locationToIndex(mouseEvent.getPoint());
                Object o = tempList.getModel().getElementAt(index);
                updateDetails(o.toString(), "associationAggregation");
            }
        });
        addToPanel(associationAggregation_liste, 300, 200, 1, 1, 2, 3);

        // Section "details"
        JLabel details = new JLabel("Détails");
        addToPanel(details, 400, 30, 2, 1, 1, 4);

        details_area = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(details_area);
        addToPanel(scrollPane, 200, 200, 2, 1, 1, 5);

    }

    /**
     * Fait un update de l'affichage des classes"
     */
    public void update() {

        String[] classes = new String[this.classes.size()];
        Iterator<String> iterClasses = this.classes.keySet().iterator();

        for (int i = 0; i < classes.length; i++) {
            classes[i] = iterClasses.next();
        }
        classes_liste.setListData(classes);
        repaint();
    }

    /**
     * Methode qui fait un update du display lorsqu'une classe est selectionnee
     *
     * @param nomClasse le nom de la classe selectionnee dans la section "Classes" du
     *                  panneau
     */
    public void updateDisplayFromClass(String nomClasse) {

        // Mise en memoire de la classe selectionnee sur le coup
        classeSelectionnee = classes.get(nomClasse);

        // Mise a jour des attributs
        HashMap<String, Attribut> mapAtt = classes.get(nomClasse).getAttributs();
        String[] list = new String[mapAtt.size()];
        Iterator<String> iterAtt = mapAtt.keySet().iterator();

        for (int i = 0; i < mapAtt.size(); i++) {
            list[i] = iterAtt.next();
        }
        attributs_liste.setListData(list);

        // Mise a jour des methodes
        HashMap<String, Methode> mapMet = classes.get(nomClasse).getMethodes();
        list = new String[mapMet.size()];
        Iterator<String> iterMet = mapMet.keySet().iterator();

        for (int i = 0; i < mapMet.size(); i++) {
            list[i] = iterMet.next();
        }
        methodes_liste.setListData(list);

        // Mise a jour des association/aggregation
        HashMap<String, Association> mapAss = classes.get(nomClasse).getAssociations();
        HashMap<String, Aggregation> mapAgg = classes.get(nomClasse).getAggregations();
        list = new String[mapAss.size() + mapAgg.size()];
        Iterator<String> iterAss = mapAss.keySet().iterator();
        Iterator<String> iterAgg = mapAgg.keySet().iterator();

        for (int i = 0; i < mapAss.size(); i++) {
            list[i] = iterAss.next();
        }
        for (int i = mapAss.size(); i < list.length; i++) {
            list[i] = iterAgg.next();
        }
        associationAggregation_liste.setListData(list);

        // Mise a jour des sous-classes
        HashMap<String, Classe> mapSousClasses = classes.get(nomClasse).getSousClasses();
        list = new String[mapSousClasses.size()];
        Iterator<String> iterSousClasses = mapSousClasses.keySet().iterator();

        for (int i = 0; i < mapSousClasses.size(); i++) {
            list[i] = iterSousClasses.next();
        }
        sousClasses_liste.setListData(list);

        // Raffraichit les détail (enlève tout)
        details_area.setText(null);
        repaint();

    }

    /**
     * Permet de faire un update dans la case "détails" lorsqu'un élément est
     * sélectionné
     *
     * @param nomSelection nom de la ligne selectionnée. Sert de clé pour chercher dans
     *                     un HashMap
     * @param origine      section dans laquelle un element a été sélectionné
     */
    public void updateDetails(String nomSelection, String origine) {

        // Variables
        String text = "";

        // Actions différentes dépendament de l'emplacement de l'origine de la
        // selection
        switch (origine) {
            case "attribut":
                clearSelections(origine);
                text = "ATTRIBUTES\n" + classeSelectionnee.getAttributs().get(nomSelection).getDetails();
                break;

            case "methode":
                clearSelections(origine);
                text = "OPERATIONS\n" + classeSelectionnee.getMethodes().get(nomSelection).getDetails();
                break;

            case "sousClasse":
                clearSelections(origine);
                text = classeSelectionnee.getSousClasses().get(nomSelection).getDetails();
                break;

            case "associationAggregation":
                clearSelections(origine);
                // Definie si c'est une aggregation ou une association avant de definir le texte a imprimer
                if (nomSelection.charAt(1) == 'R') {
                    text = "RELATION " + classeSelectionnee.getAssociations().get(nomSelection).getDetails();
                }
                if (nomSelection.charAt(1) == 'A') {
                    text = "AGGREGATION \n" + classeSelectionnee.getAggregations().get(nomSelection).getDetails();
                }
                break;
        }

        details_area.setText(text);
        repaint();
    }

    /**
     * Methode qui sert a retirer toutes les selections autres que la dernière
     * sélection
     *
     * @param origine nom de la source a ne pas désélectionner
     */
    public void clearSelections(String origine) {

        if (!origine.equals("attribut")) {
            attributs_liste.clearSelection();
        }
        if (!origine.equals("methode")) {
            methodes_liste.clearSelection();
        }
        if (!origine.equals("sousClasse")) {
            sousClasses_liste.clearSelection();
        }
        if (!origine.equals("associationAggregation")) {
            associationAggregation_liste.clearSelection();
        }
        details_area.setText("");
        repaint();
    }

    /**
     * Méthode set pour le HashMap de classes
     *
     * @param classes le HashMap de classes
     */
    public void setClasses(HashMap<String, Classe> classes) {
        this.classes = classes;
    }

    /**
     * Methode addToPanel qui considere la taille de l'objet a mettre dans le
     * gridbag display
     *
     * @param obj    l'objet a passer (JLabel, JList ou JScrollPanel)
     * @param dimX   dimension de l'objet en x
     * @param dimY   dimension de l'objet en y
     * @param width  largeur du gridbag en cases
     * @param height hauteur de l'objet en cases
     * @param X      nombre de cases en x
     * @param Y      nombre de cases en y
     */
    public void addToPanel(Object obj, int dimX, int dimY, int width, int height, int X, int Y) {

        GridBagConstraints gBC = new GridBagConstraints();
        gBC.fill = GridBagConstraints.BOTH;

        // Emplacement dans le gridbag display
        gBC.gridwidth = width;
        gBC.gridheight = height;
        gBC.gridx = X;
        gBC.gridy = Y;

        // Definie la taille et l'emplacement et ajoute l'objet au panel dans le
        // GridBag

        // Pour un JLabel...
        if (obj instanceof JLabel) {
            ((JLabel) obj).setPreferredSize(new Dimension(dimX, dimY));
            this.add((JLabel) obj, gBC);
        }
        // Pour un JList...
        else if (obj instanceof JList) {
            ((JList) obj).setPreferredSize(new Dimension(dimX, dimY));
            ((JList) obj).setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createEmptyBorder(1, 1, 1, 1), // outer
                    BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK)));
            this.add((JList) obj, gBC);
        }
        // Pour un JScrollPane...
        else if (obj instanceof JScrollPane) {
            ((JScrollPane) obj).setPreferredSize(new Dimension(dimX, dimY));
            ((JScrollPane) obj)
                    .setBorder(BorderFactory.createCompoundBorder(
                            BorderFactory.createEmptyBorder(5, 5, 5, 5), // outer								// border
                            BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK)));
            this.add((JScrollPane) obj, gBC);
        }
    }
}