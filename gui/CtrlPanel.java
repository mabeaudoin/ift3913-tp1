package gui;

import elementsClasse.Aggregation;
import elementsClasse.Association;
import elementsClasse.Classe;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * classe faisant l'ouverture et le parsing du fichier UML externe
 *
 * @author Marc-André Beaudoin
 * @author Étienne Boucher
 * @version 1.0
 * @since 2016-09-25
 */
public class CtrlPanel extends JPanel {

    // Variables
    private ViewPanel viewPanel;
    private JTextField textField;
    private HashMap<String, Classe> classes;

    /**
     * constructeur de la classe CtrPanel
     *
     * @param viewPanel le panneau de vue du contenu du fichier UML
     */
    public CtrlPanel(ViewPanel viewPanel) {

        // Organisation du JPanel
        this.viewPanel = viewPanel;
        this.setPreferredSize(new Dimension(600, 30));
        this.setLayout(new BorderLayout());

        // Affchage du nom de fichier
        textField = new JTextField("Cliquer sur le bouton pour charger un fichier...");
        textField.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK),
                BorderFactory.createEmptyBorder(0, 5, 0, 5)));
        textField.setPreferredSize(new Dimension(250, 30));

        // Bouton de chargement
        JButton loadFileButton = new JButton("Charger fichier");
        loadFileButton.addActionListener(new ActionListener() {

            // ActionListener sur le bouton
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(".ucd files", "ucd");
                fileChooser.setFileFilter(filter);
                int returnVal = fileChooser.showOpenDialog(getParent());
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    textField.setText(file.getName());
                    extractUML(file);
                }
            }
        });

        loadFileButton.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        loadFileButton.setPreferredSize(new Dimension(130, 30));

        this.add(loadFileButton, BorderLayout.WEST);
        this.add(textField, BorderLayout.CENTER);

    }

    /**
     * methode qui fait le parsing sur le fichier UML en utilisant des
     * expressions régulières
     *
     * @param file le fichier UML choisi
     */
    private void extractUML(File file) {
        try {
            // regex pour trouver un bloc CLASS
            String reClassDec = "CLASS\\s(\\w+)\\s+ATTRIBUTES[\\w\\s,:]+OPERATIONS[\\w\\s,:()]+;";
            String reAttributs = "ATTRIBUTES\\s+((\\w+\\s:\\s\\w+),?\\s+)*";
            String reMethodes = "OPERATIONS\\s+((\\w+\\s*\\(((\\w+\\s:\\s\\w+),?\\s?)*\\)\\s:\\s\\w+),?\\s+)*";
            String reDataItem = "(\\w+)\\s:\\s(\\w+)";
            String reSignature = "(\\w+)\\s*\\((((\\w+)\\s:\\s(\\w+)),?\\s?)*\\)\\s:\\s(\\w+)";

            // création des patterns pour la recherche de CLASS ainsi que ses sous-composantes.
            Pattern classe = Pattern.compile(reClassDec);
            Pattern attribut = Pattern.compile(reAttributs);
            Pattern methode = Pattern.compile(reMethodes);
            Pattern item = Pattern.compile(reDataItem);
            Pattern signature = Pattern.compile(reSignature);
            Matcher matcherClasse = classe.matcher(fromFile(file.getAbsolutePath()));

            // regex pour un bloc CLASS
            classes = new HashMap<String, Classe>();
            while (matcherClasse.find()) {
                // extraction du nom de la fonction
                String classeName = matcherClasse.group(1);

                // Création de la fonction et ajout à la table de hashage
                Classe newClasse = new Classe(classeName);
                classes.put(classeName, newClasse);

                // match du bloc d'attributs
                Matcher matcherAttribut = attribut.matcher(matcherClasse.group(0));
                matcherAttribut.find();

                // match chacune des déclaration d'attribut
                Matcher matcherCompAttribut = item.matcher(matcherAttribut.group(0));
                while (matcherCompAttribut.find()) {
                    String attrName = matcherCompAttribut.group(1);
                    String attrType = matcherCompAttribut.group(2);
                    newClasse.addAttribut(attrName, attrType);
                }

                // match d'un bloc de fonctions
                Matcher matcherFonction = methode.matcher(matcherClasse.group(0));
                matcherFonction.find();

                // match chacune des déclaration de fonction
                Matcher matcherCompFonction = signature.matcher(matcherFonction.group(0));
                while (matcherCompFonction.find()) {
                    // extraction des arguments et de leur type
                    int count = 0;
                    Matcher argsIterator = item.matcher(matcherCompFonction.group(0));
                    while (argsIterator.find()) {
                        count++;
                    }
                    String[][] args = new String[count][2];
                    argsIterator.reset();
                    for (count = 0; argsIterator.find(); count++) {
                        args[count][0] = argsIterator.group(1);
                        args[count][1] = argsIterator.group(2);
                    }
                    // extraction des nom et type de la fonction
                    String nom = matcherCompFonction.group(1);
                    String type = matcherCompFonction.group(matcherCompFonction.groupCount());

                    // ajout de la fonction à la classe nouvellement créée
                    newClasse.addMethode(nom, type, args);
                }
            }
            viewPanel.setClasses(classes);
            viewPanel.update();

            // regex pour un bloc GENERALIZATION
            String reGener = "GENERALIZATION\\s(\\w+)\\s+SUBCLASSES\\s(\\w+,?\\s?)+\\s+;";

            // Création des Patterns
            Pattern gener = Pattern.compile(reGener);
            Pattern subClasses = Pattern.compile("SUBCLASSES\\s[\\w\\s,]+");
            Pattern nomClasse = Pattern.compile("\\s(\\w+)");

            // match des blocs generalization
            Matcher matcherGener = gener.matcher(fromFile(file.getAbsolutePath()));
            while (matcherGener.find()) {
                // extraction de la classe parent
                Classe parent = classes.get(matcherGener.group(1));

                // extraction des sous-classes et ajout au parent
                Matcher matcherSubClasse = subClasses.matcher(matcherGener.group(0));
                matcherSubClasse.find();
                Matcher matcherNom = nomClasse.matcher(matcherSubClasse.group(0));
                while (matcherNom.find()) {
                    parent.addSousClasse(classes.get(matcherNom.group(1)));
                }
            }

            // regex pour un bloc AGGREGATION
            String reAggreg = "AGGREGATION\\s+CONTAINER\\s+CLASS\\s(\\w+)\\s(\\w+)\\s+PARTS[\\s\\w,]+;";

            // Création des Patterns
            Pattern Aggreg = Pattern.compile(reAggreg);
            Pattern parts = Pattern.compile("PARTS[\\s\\w,]+");
            Pattern declaration = Pattern.compile("CLASS\\s(\\w+)\\s(\\w+)");

            // match des blocs AGGREGATION
            Matcher matcherAggreg = Aggreg.matcher(fromFile(file.getAbsolutePath()));
            while (matcherAggreg.find()) {
                // compte du nombre de PARTS
                Matcher matcherParts = parts.matcher(matcherAggreg.group(0));
                matcherParts.find();
                Matcher matcherDeclaration = declaration.matcher(matcherParts.group(0));
                int count = 0;
                while (matcherDeclaration.find()) {
                    count++;
                }
                // création d'un tableau de multiplicités
                String[] multiplicities = new String[count + 1];

                // extraction de la classe container et stockage de sa multiplicité
                Classe container = classes.get(matcherAggreg.group(1));
                multiplicities[0] = matcherAggreg.group(2);

                // extraction des PARTS et stockage des multiplicités
                Classe[] partsClasses = new Classe[count];
                matcherDeclaration.reset();
                count = 1;
                while (matcherDeclaration.find()) {
                    partsClasses[count - 1] = classes.get(matcherDeclaration.group(1));
                    multiplicities[count] = matcherDeclaration.group(2);
                    count++;
                }
                Aggregation aggregation = new Aggregation(container, partsClasses, multiplicities);
                container.addAggregation(aggregation);
                for (Classe c : partsClasses) {
                    c.addAggregation(aggregation);
                }
            }

            // regex pour un bloc RELATION
            String reAsso = "RELATION\\s(\\w+)\\s+ROLES\\s+CLASS\\s(\\w+)\\s(\\w+),\\s+CLASS\\s(\\w+)\\s(\\w+)\\s+;";

            // Création du Pattern des blocs RELATION
            Pattern Asso = Pattern.compile(reAsso);

            // match des blocs RELATION
            Matcher matcherAsso = Asso.matcher(fromFile(file.getAbsolutePath()));
            while (matcherAsso.find()) {
                // extraction de l'étiquette
                String etiquette = matcherAsso.group(1);

                // création d'un tableau de multiplicités
                String[] multiplicities = new String[2];

                // extraction des classes et stockage de leur multiplicité
                Classe prem = classes.get(matcherAsso.group(2));
                multiplicities[0] = matcherAsso.group(3);
                Classe sec = classes.get(matcherAsso.group(4));
                multiplicities[1] = matcherAsso.group(5);

                // création de l'association et son assignation aux classes
                Association association = new Association(etiquette, prem, sec, multiplicities);
                prem.addAssociation(association);
                sec.addAssociation(association);
            }

        } catch (IOException e) {

        }
    }

    /**
     * lecture du fichier externe. Nous avons copié la fonction ci-dessous à
     * l'adresse suivante:
     * http://www.java-tips.org/java-se-tips-100019/37-java-util-regex/1716-how-
     * to-apply-regular-expressions-on-the-contents-of-a-file.html
     */
    public CharSequence fromFile(String filename) throws IOException {
        FileInputStream input = new FileInputStream(filename);
        FileChannel channel = input.getChannel();

        // Create a read-only CharBuffer on the file
        ByteBuffer bbuf = channel.map(FileChannel.MapMode.READ_ONLY, 0, (int) channel.size());
        CharBuffer cbuf = Charset.forName("8859_1").newDecoder().decode(bbuf);
        return cbuf;
    }
}