package elementsClasse;

import java.util.HashMap;
import java.util.Iterator;

/**
 * classe servant à stocker les informations d'une classe (ou sous-classe) d'un
 * schéma UML
 * 
 * @author Marc-André Beaudoin
 * @author Étienne Boucher
 * @version 1.0
 * @since 2016-09-25
 *
 */
public class Classe {
	private String nom;
	private HashMap<String, Attribut> attributs;
	private HashMap<String, Methode> methodes;
	private HashMap<String, Classe> sousClasses;
	private HashMap<String, Aggregation> aggregations;
	private HashMap<String, Association> associations;

	/**
	 * constructeur de la classe "Classe"
	 * 
	 * @param nom
	 *            nom de la classe a créer
	 */
	public Classe(String nom) {
		this.nom = nom;
		attributs = new HashMap<String, Attribut>();
		methodes = new HashMap<String, Methode>();
		sousClasses = new HashMap<String, Classe>();
		aggregations = new HashMap<String, Aggregation>();
		associations = new HashMap<String, Association>();
	}

	/**
	 * Methode qui retourne les détails a afficher pour les sous-classes
	 *
	 * @return le string montrant les details de la classe
	 */
	public String getDetails() {

		String text = "CLASS " + nom + "\n\n";

		// Ajoute la liste d'attributs
		if (attributs.size() != 0) {
			text += "ATTRIBUTES\n";
			Iterator<String> iter = attributs.keySet().iterator();
			for (int i = 0; i < attributs.size(); i++) {
				String key = iter.next();
				text += attributs.get(key).getDetails() + "\n";
			}
		}

		// Ajoute la liste de methodes
		if (methodes.size() != 0) {
			text += "METHODS\n";
			Iterator<String> iter = methodes.keySet().iterator();
			for (int i = 0; i < methodes.size(); i++) {
				String key = iter.next();
				text += methodes.get(key).getDetails() + "\n";
			}
		}

		// Ajoute la liste de sous-classes
		if (sousClasses.size() != 0) {
			text += "GENERALIZATION " + nom + "\n\tSUBCLASSES ";
			Iterator<String> iter = sousClasses.keySet().iterator();
			// Les dremiers items avec un "," a la fin
			for (int i = 0; i < sousClasses.size() - 1; i++) {
				String key = iter.next();
				text += sousClasses.get(key).getNom() + ", ";
			}
			// Le dernier item sans ","
			if (sousClasses.size() != 0) {
				String key = iter.next();
				text += sousClasses.get(key).getNom();
			}
		}

		// Ajoute la liste d'associations
		if (associations.size() != 0) {
			Iterator<String> iter = associations.keySet().iterator();
			// Les dremiers items avec un "," a la fin
			for (int i = 0; i < associations.size(); i++) {
				String key = iter.next();
				text += "RELATION " + associations.get(key).getDetails() + "\n";
			}
		}

		// Ajoute la liste d'aggregations
		if (aggregations.size() != 0) {
			Iterator<String> iter = aggregations.keySet().iterator();
			// Les dremiers items avec un "," a la fin
			for (int i = 0; i < aggregations.size(); i++) {
				String key = iter.next();
				text += "AGGREGATION \n" + aggregations.get(key).getDetails() + "\n";
			}
		}
		return text;
	}

	@Override
	public String toString() {
		return nom;
	}

	/**
	 * retourne le nom de la classe
	 * 
	 * @return nom de la classe
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * retourne les attributs de la classe dans le modèle UML
	 * 
	 * @return attributs de la classe
	 */
	public HashMap<String, Attribut> getAttributs() {
		return attributs;
	}

	/**
	 * retourne les méthodes de la classe dans le modèle UML
	 * 
	 * @return méthodes de la classe
	 */
	public HashMap<String, Methode> getMethodes() {
		return methodes;
	}

	/**
	 * retourne les sous-classes de la classe dans le modèle UML
	 * 
	 * @return sous-classes de la classe
	 */
	public HashMap<String, Classe> getSousClasses() {
		return sousClasses;
	}

	/**
	 * retourne les aggregations de la classe dans le modèle UML
	 * 
	 * @return aggregations de la classe
	 */
	public HashMap<String, Aggregation> getAggregations() {
		return aggregations;
	}

	/**
	 * retourne les associations de la classe dans le modèle UML
	 * 
	 * @return associations de la classe
	 */
	public HashMap<String, Association> getAssociations() {
		return associations;
	}

	/**
	 * ajoute un attribut à la classe
	 * 
	 * @param nom
	 *            nom de l'attribut à ajouter
	 * @param type
	 *            type de l'attribut à ajouter
	 */
	public void addAttribut(String nom, String type) {
		attributs.put(type + ' ' + nom, new Attribut(nom, type));
	}

	/**
	 * ajoute une méthode à la classe
	 * 
	 * @param nom
	 *            nom de la méthode
	 * @param type
	 *            type de la méthode
	 * @param args
	 *            arguments de la méthode
	 */
	public void addMethode(String nom, String type, String[][] args) {

		String methode = "";
		if (!type.equals("void"))
			methode += (type + " ");
		methode += (nom + "(");

		for (int i = 0; i < args.length; i++) {
			methode += (i == 0) ? args[0][1] : (", " + args[i][1]);
		}
		methode += ")";
		methodes.put(methode, new Methode(nom, type, args));
	}

	/**
	 * ajoute une sous-classe à la classe
	 * 
	 * @param sousClasse
	 *            la sous-classe à ajouter
	 */
	public void addSousClasse(Classe sousClasse) {
		sousClasses.put(sousClasse.getNom(), sousClasse);
	}

	/**
	 * ajoute une aggrégation à la classe
	 * 
	 * @param aggregation
	 *            aggrégation à ajouter
	 */
	public void addAggregation(Aggregation aggregation) {
		if (this == aggregation.getContainer()) {
			String[] noms = aggregation.getPartsName();
			for (int i = 0; i < noms.length; i++) {
				String affichage = "(A) P_" + noms[i];
				aggregations.put(affichage, aggregation);
			}
		} else {
			String affichage = "(A) C_" + aggregation.getContainer().getNom();
			aggregations.put(affichage, aggregation);
		}
	}

	/**
	 * ajouter une association à la classe
	 * 
	 * @param association
	 *            association à ajouter
	 */
	public void addAssociation(Association association) {
		String affichage = "(R) ";
		if (this == association.getPremRole()) {
			affichage += association.getEtiquette();
		} else {
			affichage += "inv_" + association.getEtiquette();
		}

		associations.put(affichage, association);
	}
}