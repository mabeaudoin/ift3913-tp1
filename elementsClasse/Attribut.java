package elementsClasse;

/**
 * classe qui définit un attribut pour une classe dans un modèle UML
 * 
 * @author Marc-André Beaudoin
 * @author Étienne Boucher
 * @version 1.0
 * @since 2016-09-25
 * 
 */
public class Attribut {

	// Variables de classe
	private String nom, type;

	/**
	 * constructeur de la classe Attribut
	 * 
	 * @param nom
	 *            nom de l'attribut
	 * @param type
	 *            type de l'attribut
	 */
	public Attribut(String nom, String type) {
		this.nom = nom;
		this.type = type;
	}

	/**
	 * retourne les détails à afficher de la classe Attribut
	 * 
	 * @return les détails de l'attribut
	 */
	public String getDetails() {
		return "\t" + nom + " : " + type;
	}

	@Override
	public String toString() {
		return type + ' ' + nom;
	}
}
