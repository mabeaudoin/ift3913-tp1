package elementsClasse;

/**
 * classe qui définit une association pour une classe dans un modèle UML
 * 
 * @author Marc-André Beaudoin
 * @author Étienne Boucher
 * @version 1.0
 * @since 2016-09-25
 * 
 */
public class Association {

	// Variables de la classe
	private String etiquette;
	private Classe premRole, secRole;
	private String[] multiplicities;

	/**
	 * constructeur de la classe Association
	 * 
	 * @param etiquette
	 *            étiquette de la classe
	 * @param premRole
	 *            classe primaire
	 * @param secRole
	 *            classe secondaire
	 * @param multiplicities
	 *            multiplicité des premRole ([0]) et secRole([1])
	 */
	public Association(String etiquette, Classe premRole, Classe secRole, String[] multiplicities) {
		this.etiquette = etiquette;
		this.premRole = premRole;
		this.secRole = secRole;
		this.multiplicities = multiplicities;
	}

	/**
	 * retourne les détails à afficher de la classe Association
	 * 
	 * @return les détails de l'association
	 */
	public String getDetails() {
		return etiquette + "\n\tROLES\n\tCLASS " + premRole + " " + multiplicities[0] + " ,\n\tCLASS " + secRole + " "
				+ multiplicities[1];
	}

	/**
	 * retourne l'étiquette de la classe Association
	 * 
	 * @return étiquette de la classe
	 */
	public String getEtiquette() {
		return etiquette;
	}

	/**
	 * retourne la classe primaire de l'association
	 * 
	 * @return classe primaire
	 */
	public Classe getPremRole() {
		return premRole;
	}
}