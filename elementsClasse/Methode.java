package elementsClasse;

/**
 * classe qui définit une méthode pour une classe dans un modèle UML
 * 
 * @author Marc-André Beaudoin
 * @author Étienne Boucher
 * @version 1.0
 * @since 2016-09-25
 * 
 */
public class Methode {
	private String nom;
	private String type;
	private String[][] args;

	/**
	 * constructeur de la classe Methode
	 * 
	 * @param nom
	 *            nom de la méthode
	 * @param type
	 *            type de la méthode
	 * @param args
	 *            arguments de la méthode
	 */
	public Methode(String nom, String type, String[][] args) {
		this.nom = nom;
		this.type = type;
		this.args = args;
	}

	/**
	 * retourne les détails à afficher de la méthode
	 * 
	 * @return les détails de la méthode
	 */
	public String getDetails() {

		String txtArgs = "";
		for (int i = 0; i < args.length; i++) {
			txtArgs += (i == 0) ? args[0][1] : (", " + args[i][1]);
		}
		return "\t" + nom + "(" + txtArgs + ") : " + type;
	}

	@Override
	public String toString() {
		String methode = (type.equals("void")) ? "" : (type + ' ') + nom + "(";
		for (int i = 0; i < args.length; i++) {
			methode += (i == 0) ? args[0][1] : (", " + args[i][1]);
		}
		return methode + ")";
	}
}