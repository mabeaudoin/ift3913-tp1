package elementsClasse;

/**
 * classe qui définit une aggrégation pour une classe dans un modèle UML
 * 
 * @author Marc-André Beaudoin
 * @author Étienne Boucher
 * @version 1.0
 * @since 2016-09-25
 * 
 */
public class Aggregation {
	private Classe container;
	private Classe[] parts;
	private String[] multiplicities;

	/**
	 * Constructeur de la classe Aggregation
	 * 
	 * @param container
	 *            classe primaire
	 * @param parts
	 *            classes aggrégés
	 * @param multiplicities
	 *            multiplicité des classes aggrégées
	 */
	public Aggregation(Classe container, Classe[] parts, String[] multiplicities) {
		this.container = container;
		this.parts = parts;
		this.multiplicities = multiplicities;
	}

	/**
	 * retourne les détails à afficher de la classe Association
	 * 
	 * @return les détails des aggrégations sur la classe
	 */
	public String getDetails() {
		// Premiere section du texte
		String text = "CONTAINER\n\tCLASS " + container + " " + multiplicities[0] + "\nPARTS\n";

		// Premiere section de l'enumeration des parts (ajoute un "," en bout de
		// ligne)
		for (int i = 0; i < parts.length - 1; i++) {
			text += "\tCLASS " + parts[i] + " " + multiplicities[i + 1] + ",";
		}
		// Derniere ligne de l'enumeration des parts (sans ",")
		if (parts.length != 0)
			text += "\tCLASS " + parts[parts.length - 1] + " " + multiplicities[parts.length];

		return text;
	}

	/**
	 * retourne la classe primaire
	 * 
	 * @return classe primaire
	 */
	public Classe getContainer() {
		return container;
	}

	/**
	 * retourne les noms des classes aggrégées
	 * 
	 * @return tableau des noms des classes aggrégées
	 */
	public String[] getPartsName() {

		String[] noms = new String[parts.length];
		for (int i = 0; i < parts.length; i++) {
			noms[i] = parts[i].getNom();
		}
		return noms;
	}
}